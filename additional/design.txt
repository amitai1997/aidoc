*Introduction*
    This service provides access to statistics about the spread of the COVID-19 virus over the globe.


*Design Overview*

    To my understanding, the challenge is not only a logical challenge, but also comes to test practical knowledge in writing code and implementing a server side.
    Therefore, I decided to develop a server side in Python that allows the user access to REST API that generate dynamic http requests for the data stored in the DB, and allow adding logic and filtering capabilities for the data.
    After talking on the phone with Rotem, I realized that the project requirements are to present a CLI to the user, so I decided to "wrap" the API in a container, and using Python scripts to run the program as a CLI.
    The entire project is powered by a docker compose that can be run in any environment.

*Technologies*

    Server side - Python(Django)
    Cli scripts - Pythpn
    DB - postgres
    Containers - Docker compose
    Postman for API testing platform 
    I managed my code in Gitlab

*Logical Architecture*

    I extracted the relevant data from the csv file, after the user copies the information from the file to the DB the user can enter the CLI container.
    A menu is displayed for the user:
    1. View all available information on all countries (paginated to 1000 results for each page)
    2. Displays total cases for a particular country in the last X days (allows the user to select the number of days)
    3. The highest number of total cases in the X countries (allows the user to select the number of countries)
    4. Exit the menu

    For each selection the program generates an URL, and send an API request with that url to the Backend.
    Then, the program get the result that the backend returns, and display it to the user.
    The menu is shown again until the user decides to exit.

    There are three main files:
    menu.py - Displays and manages the menu
    visualization.py - Print the information to the console user
    main.py - Uses the other two files to run the program

*Conclusion*

    Although the project took me more than 5 hours to create it but I enjoyed the process and also learned new things along the way. I hope you'll like it
