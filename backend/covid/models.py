from django.db import models

from django.db import models


class Covid(models.Model):
    locationn = models.CharField(max_length=50)
    case_date = models.CharField(max_length=50)
    total_cases = models.IntegerField(blank=True, null=True)
    new_cases = models.IntegerField(blank=True, null=True)
