from .models import Covid
from .serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import date, timedelta
from rest_framework.pagination import LimitOffsetPagination


class covidList(APIView, LimitOffsetPagination):
    def get(self, request, format=None):
        objects = Covid.objects.all()
        results = self.paginate_queryset(objects, request, view=self)
        serializer = covidSerializer(results, many=True)

        return self.get_paginated_response(serializer.data)


class covidByLocation(APIView):
    def get(self, request, location, format=None):
        snippet = Covid.objects.filter(locationn__iexact=location)
        serializer = covidSerializer(snippet, many=True)
        return Response(serializer.data)


class covidByLocationRetro(APIView):
    def get(self, request, location, days, format=None):
        snippet = Covid.objects.filter(locationn__iexact=location).filter(
            case_date__gte=(date.today() - timedelta(int(days))))

        serializer = covidNewCasesSerializer(snippet, many=True)
        return Response(serializer.data)


class TotalCases(APIView):
    def get(self, request, max, format=None):
        exclude_list = ["World", "High income",
                        "Europe", "Asia", "European Union", "Upper middle income", "North America", "Lower middle income"]
        lastDayOnDB = date(2022, 6, 3)

        all_locations_without_unions = Covid.objects.exclude(locationn__in=exclude_list).exclude(
            total_cases__isnull=True)

        all_location_last_day = all_locations_without_unions.filter(
            case_date__gte=lastDayOnDB)

        total_cases_locations = all_location_last_day.order_by(
            "-total_cases")[:max]

        serializer = covidTotalCasesSerializer(
            total_cases_locations, many=True)
        return Response(serializer.data)
