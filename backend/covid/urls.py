from django.urls import include, path
from rest_framework import routers
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('all/', views.covidList.as_view()),
    path('location/<str:location>/', views.covidByLocation.as_view()),
    path('location/<str:location>/<int:days>/',
         views.covidByLocationRetro.as_view()),
    path('total/<int:max>/',
         views.TotalCases.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
