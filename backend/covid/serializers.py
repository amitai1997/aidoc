from .models import Covid
from rest_framework import serializers


class covidSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Covid
        fields = ['locationn', 'case_date', 'total_cases', 'new_cases']


class covidNewCasesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Covid
        fields = ['case_date', 'new_cases']


class covidTotalCasesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Covid
        fields = ['locationn', "total_cases"]
