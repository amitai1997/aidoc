#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

# python manage.py flush --no-input
python manage.py makemigrations
python manage.py migrate

# psql -U django -d django_dev -c "COPY covid(locationn, case_date, total_cases, new_cases) FROM '/additional/aidoc_covid.csv' DELIMITER ',' CSV HEADER;"

exec "$@"
