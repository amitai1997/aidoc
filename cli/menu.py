
import sys


class CovidSelection:
    suffix = None

    def action(self):
        pass


class ListAllData(CovidSelection):
    suffix = "all/"

    def action(self):
        return self.suffix


class locationByRetroDate(CovidSelection):
    suffix = "location"

    def action(self):
        location = input("Enter Location: ")
        retroDays = input("How many days back?:  ")

        return self.suffix + "/" + location + "/" + retroDays + "/"


class LargestTotalCasesByCountry(CovidSelection):
    suffix = "total"

    def action(self):
        countryCount = input("The amount of countries you would like to see: ")

        return self.suffix + "/" + countryCount + "/"


class Menu:
    host = "backend"
    # host = "localhost"

    basicUrl = "http://" + host + ":8000/covid/"

    def __init__(self):
        self.choices = {
            "1": ListAllData(),
            "2": locationByRetroDate(),
            "3": LargestTotalCasesByCountry(),
            "4": self.quit
        }

    def display_menu(self):
        print("""
            1. Show all data (paginated)
            2. Country days retro 
            3. Top total cases
            4. Quit
        """)

    def run(self):
        '''Display the menu and respond to choices.'''
        # while True:
        self.display_menu()
        choice = input("Enter an option: ")
        if choice in self.choices.keys():

            if choice == list(self.choices.keys())[-1]:
                self.quit()

            selection = self.choices.get(choice)
            suffix = selection.action()

            return self.basicUrl + suffix
        else:
            print("\n{0} is not a valid choice".format(choice))

    def quit(self):
        print("Thank you for using your covid menu today.")
        sys.exit(0)
