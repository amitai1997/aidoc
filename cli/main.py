import requests
import menu
import visualization


def main():
    covidMenu = menu.Menu()
    while (True):
        covidHttpRquest = covidMenu.run()

        try:
            result = requests.get(covidHttpRquest)
            visualization.printJson(result.content)
        except:
            print("\nPlease try again!")


if __name__ == '__main__':
    main()
