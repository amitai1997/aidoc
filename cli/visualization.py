import json
import pandas as pd
pd.set_option('display.max_rows', 5000)


def printJson(rawJson):

    my_json = rawJson.decode('utf8').replace("'", '"')
    try:
        my_data = json.loads(my_json)
        if 'results' in my_data:
            print(pd.DataFrame(data=my_data['results']))
        else:
            print(pd.DataFrame(data=my_data))
    except:
        print("\nPlease try again!")
        return
